# project_name

author_name \
current_date

This is my current data-analysis project template.

## Usage

I use a [bash script](https://gitlab.com/famuvie/newprj) that sets up a new project for me based on this template.

Otherwise, simply [download](https://forgemia.inra.fr/famuvie/project-template/-/archive/master/project-template-master.zip) this repository and extract into your projects' directory, change the directory name and start editing files.

The project structure is as follows:

- `data/`: Original data files, as they were received. 

    I don't touch them at all. I may even set read-only permissions on them. I typically track versions (even if sometimes they are binary files) for reproducibility. Mind about confidentiality issues if the repository is to be made public.

- `doc/`: Supporting documents, papers, etc. that are not produced by you, but are useful or needed for reference.

    Binary documents in PDF or doc(x) formats are untracked (edit `doc/.gitignore` as needed). However there is a `Notes.md` for keeping a project log and a `Makefile` to compile the notes to PDF format.
    
- `public/`: Dissemination material in HTML format (reports, slides). Everything here will be published automatically on line by _GitLab Pages_.

- `reports/`: Analysis reports or slides in PDF format. Different from `public/` in purpose as well. `public/` is for dissemination and collaboration on line, whereas `reports/` is for PDF documents, targeted to others or internal.

- `src/`: Source code for everything. Functions, figures, reports, analyses. 

    The `.Rmd` file produces a RMarkdown report (for `public/` or `reports/` or both). You can make as many of these as you wish.
    
    Write functions into `functions.R`. The rest are ancilliary files. Write \LaTeX macros into `before_body.tex` and specific \LaTeX packages into `preamble.tex`. Include bibliographic references in BibTeX format into the `.bib` file. You can change the bibliographic style in the `.csl` file by whatever style you prefer.

- The `.Rproj` file defines a RStudio project with some preferences and need not be touched.

- `.gitignore` specifies which files are not to be tracked.

- `.gitlab-ci.yml` configures GitLab's continuous integration in order to produce GitLab Pages based from `README.md` and files within `public/`.

- `README.md` is the current file and should be edited to give an overview your project. It will become the index page of the GitLab Pages website, and can include links to compiled reports for dissemination.

    Sometimes we want to display some results or run some code to produce the index page. In that case, use `usethis::use_readme.rmd()`.

- `_targets.R` is the __main file of the project__. It consists of a list of _targets_ that need to be computed using functions and code therein. These _targets_ are R objects than can be referred to from within the `Rmd` files to produce the reports.

    Some of the targets load or clean up the data sets producing intermediate objects that are used by others in turn, others make figures and others are meant to compile the reports themselves.
